<div align="center">

![VERY Temporary Logo! Damn I Need To Hire An Actual Artist...](compegodot/assets/splash-screen-ingame.png)

[![](https://img.shields.io/badge/matrix-chat%20here!-brightgreen)](https://matrix.to/#/#competitive-godot-fps:matrix.org) [![](https://img.shields.io/badge/version-0.8.0--alpha-blue)](https://gitlab.com/solver-orgz/project-solver)

This project is a First Person Shooter that aims to be the most versatile and fun tactical shooter there is! Made with Godot Engine, it follows concepts based on the proposal at [`documents/PROPOSAL.md`](documents/PROPOSAL.md). Developers, artists, playtesters, and even beginners are more than welcome to contribute!

Created by Tengku Izdihar.

[Create your first proposal/issue here!](https://gitlab.com/solver-orgz/project-solver/-/issues/new)

</div>

# Table Of Contents
- [Table Of Contents](#table-of-contents)
- [Development Status](#development-status)
- [Screenshot](#screenshot)
- [How to Contribute](#how-to-contribute)
- [Installing and Building The Game](#installing-and-building-the-game)
- [Roadmap](#roadmap)
  - [**Project Solver: Training** (CURRENT PHASE)](#project-solver-training-current-phase)
  - [**Project Solver: 1v1**](#project-solver-1v1)
  - [**Project Solver: 5v5**](#project-solver-5v5)
- [Technical FAQ](#technical-faq)

# Development Status

![Dev Badge](https://img.shields.io/badge/Development%20-Active-brightgreen)

Update: 30-March-2023 Godot 4 is here and I'm on my way to implement a multiplayer feature. I'm working on this project as actively as I can be, in my free time from work. Feel free to reach out by going to reach me [our community here](https://matrix.to/#/#competitive-godot-fps:matrix.org).

Because of the importance of Godot 4.0 and the breaking changes we would need to handle once we're going 4.0 eventually, it's decided that we will pause any large features for this project such as multiplayer support, until Godot 4.0 is stable. In the meantime, project owner is working on other project such as,
* [Writer Heaven](https://gitlab.com/tengkuizdihar/writer-heaven), a web application made for writers to publish and manage their stories.
* [Link Level Zero](https://tengkuizdihar.gitlab.io/tags/llz/), a web novel about a guy with his wife doing spy stuff. Magic is also involved I guess.

For any question, you can contact the project owner on matrix which is listed in https://tengkuizdihar.gitlab.io/contact/. **This project uses GIT-LFS**, make sure your environment has it.

# Screenshot

This screenshot is last updated Sunday, March 20, 2022 8:46:54 PM GMT+07:00.

![Gameplay](documents/assets/gameplay.png)
![Main Menu](documents/assets/gameplay_main_menu_select_level.png)
![Pause Menu - Option](documents/assets/gameplay_pause_menu_option.png)
![Gameplay](documents/assets/gameplay_sniper_show_off.png)

# How to Contribute

Please read [documents/CONTRIBUTING.md](documents/CONTRIBUTING.md) for further assistance.

# Installing and Building The Game

Head over to [compogodot/REAMDE.md](compegodot/README.md) for more information. The game's source code is located under `compegodot/`.

# Roadmap

Right now, the project does have milestones and an end game for the lifetime of the project. The endgame itself is not the end of the project, but rather the start of a new era where every single feature is implemented, at least for a prototype. The project is called "Project Solver". "Project Solver" is a game that would evolve using the effort of the developer and or community, until the "perfect" or at least "usable" game could exist. Below is the list of games and their features.

## **Project Solver: Training** (CURRENT PHASE)

The first game in Project Solver that's all about aiming and training. It features customizations and offline capabilities for those who wants it. The game itself would focus on self improvement in the aiming department and will feature source-like movement as its main locomotion system. The features intended to be in the game is listed below.

- [x] Variety of target to shot at
- [x] Different weapons for different needs
  - [ ] Guns
    - [x] General Concepts
      - [x] Spray Pattern
      - [x] Recoil
      - [x] Inaccuracy (e.g. based on movement)
      - [x] Bullet Penetration
    - [x] Automatic Weapon (e.g. AK-47)
    - [x] Semi-Automatic Weapon (e.g. M9 Pistol)
    - [x] Scoped Semi-Automatic Weapon (e.g. Sniper Rifle)
    - [ ] Shotgun
  - [ ] Utilities
    - [x] General Concepts
      - [x] Right Click vs Left Click Throw
      - [x] Timed Detonation
    - [x] Frag Grenade
    - [x] Smoke Grenade
    - [x] Flash Grenade
    - [ ] Incendiary Grenade
- [x] Training Methods
  - [x] Spray Practice
  - [x] Moving Target Practice
  - [x] Time Attack Practice (shoot target through corridor as fast as possible)
- [x] Movement system that's inspired by the Counter Strike series
  - [x] Crouch Jumping
  - [x] Bunny Hopping
  - [x] Air Strafing
  - [x] Counter Strafing
- [ ] Sound Design
  - [ ] footsteps
    - [ ] adaptive (many kinds of surfaces)
    - [x] static (only one kind of surface)
  - [ ] Etc.
- [ ] Animation
- [x] Light weight experience that's not only targeting the toaster, but also your beefed up $500,000, NASA made, super computer
- [ ] Easy to use UI customization that also includes an export and import function, for portability.
- [ ] Importing mouse sensitivity from other games
  - [ ] CSGO
  - [ ] Valorant
  - [ ] Call Of Duty

## **Project Solver: 1v1**

The second game in Project Solver that's all about 1v1 PVP. This game will include every single feature of The Aim Solver but now with a lot more variety of weapons and game modes. In multiplayer, the server will have the highest authority on what can or can't be done. This attributes will contribute to make the game much safer from cheaters and the like. The features intended to be in the game will be,

- [ ] Multiplayer that features 2 players fighting against each other.
- [ ] Replay features that's modular and can be copied by the user for save keeping purposes.
- [ ] Matchmaking based on the reputation and skill of the player.
- [ ] Statistics and data that the user can use to know which skill they need to improve upon.

## **Project Solver: 5v5**

The endgame of Project Solver that's all about 5v5 PVP. This game will also include every single feature of its predecessor and now with a working competitive mode that will clash two teams made out of 5 players doing their best to win.

- [ ] A better more robust matchmaking that's suited for a 10 player per match.
- [ ] A nicer and universal spectating system so other could use their client to do so.
- [ ] Delay based spectating for competitive matches.
- [ ] A more flexible and transparent skill assignments.

# Technical FAQ

- **I'm on NixOS and I can't run any game that's exported from Godot!**

  Run the game by going into root directory where the game is, and then use `steam-run <location of the game file>`. For example, `steam-run ./game.x86_64`.

  This of course happens because of how NixOS is structured and it seems like Godot does rely on some library such as `/lib64/ld-linux-x86-64.so.2` (in which every single 64 bit linux distribution should have).
