class_name Player
extends CharacterBody3D

###########################################################
# Interfaces
###########################################################

@onready var i_health = $IHealth
@onready var i_flash = $IFlash
@onready var i_smoke_screen = $ISmokeScreen

###########################################################
# Variables
###########################################################

# Is a constant used by the kinematic for making the player stay checked the ground.
# For example: It's used so that the player wouldn't fly unchecked when going up and down
#              ramps.
const MAGIC_ON_GROUND_GRAVITY = 2.5 # m/s

# A constant used to mark how much time is the maximum for a person to be flashed.
# For example: A man is flashed for 10 seconds then flashed again when there's
#              5 seconds left for the flash to be active. The flash will not be active for
#              15 seconds, but for 10.
const FLASH_MAXIMUM_DURATION = 5.0

# Used to mark the maximum distance of flash from the player to affect them
const FLASH_MAXIMUM_DISTANCE = 100.0

# Used to determine how close the angle of flash must be to the camera to work.
# For example, a flash that's happening behind the player will not blind them.
const FLASH_MAXIMUM_ANGLE = deg_to_rad(110)

# A constant used to determine the flash overlay color
const DEFAULT_FLASH_COLOR = Color(1,1,1,1)

# A constant used to determine the color of "smoked" vision
const DEFAULT_SMOKED_COLOR = Color(0.5,0.5,0.5, 1)

# Used for the height of a player to fall to make an audible sound.
# This value is coming from experiment so feel free to change to a more appropriate one in the future.
const DEFAULT_AUDIBLE_FALL_HEIGHT = 3.25

# is_player is used for marking whether an object is a player or not.
#
# The reason it needs a marker instead of checking the class itself is
# because Godot 3.x has a big cyclic dependency problems and it could ruin other scripts.
@onready var is_player = true

@onready var body = $Body
@onready var headlimit_raycast = $HeadLimitRayCast
@onready var normal_input_direction = Vector3.ZERO # will be changed by outside actors
@onready var parent_pivot = $ParentPivot
@onready var camera = $ParentPivot/Pivot/Camera3D
@onready var pivot = $ParentPivot/Pivot
@onready var gui = $ParentPivot/Pivot/Camera3D/PlayerGui
@onready var mouse_sensitivity = 0.0008  # radians/pixel, TODO: refactor to game settings
@onready var player_model = $ParentPivot/MannequinMasculine

#-- Gun Variables --#
@onready var gun_container = $ParentPivot/Pivot/Camera3D/GunContainer
@onready var weapon: GenericWeapon = $ParentPivot/Pivot/Camera3D/GunContainer/KF1
@onready var current_weapon_slot = Global.WEAPON_SLOT.MELEE
@onready var last_weapon_slot_used = Global.WEAPON_SLOT.MELEE
@onready var weapons = {
	Global.WEAPON_SLOT.PRIMARY: null,
	Global.WEAPON_SLOT.SECONDARY: null,
	Global.WEAPON_SLOT.MELEE: $ParentPivot/Pivot/Camera3D/GunContainer/KF1,
	Global.WEAPON_SLOT.UTILITY: null
}

#-- The currently used max velocity for movement input --#
var current_max_movement_velocity = max_run_velocity

#-- The desired movement_velocity stored for acceleration purposes. --#
var desired_movement_velocity = Vector3()

#-- The the max XZ velocity when in the air --#
var max_air_velocity = 0

#-- Flag for crouching --#
var is_crouching = false
var debug_position_one_frame_ago = Vector3.ZERO
var held_weapon = null

#-- Jumping Variables --#
var last_time_on_ground = 0
var current_max_airborne_height = 0

#-- Weapon sway variables --#
@onready var gun_container_original_rotation = Vector2(gun_container.rotation_degrees.x, gun_container.rotation_degrees.y)
var mouse_turn_max_sensitivity = 1

#-- Flashed variables --#
@onready var flash_overlay = $ParentPivot/Pivot/Camera3D/FlashOverlay
var flash_remaining_second = 0.0

#-- Smoke Screen Variables --#
@onready var smoke_overlay = $ParentPivot/Pivot/Camera3D/SmokeOverlay

@onready var gun_container_original_transform = gun_container.transform
@onready var pivot_original_local_translation = pivot.transform.origin
@onready var body_original_local_translation = body.transform.origin
@onready var body_original_height = body.shape.height
@onready var crouch_height = 3.375

#-- Sound Variables -- #
@onready var walking_audio_player = $RandomWalkingAudioPlayer3D
@onready var dropping_audio_player = $RandomDroppingAudioPlayer3D

@export var auto_bhop: bool = true
@export var disable_movement: bool = false
@export var disable_weapon_drop: bool = false
@export var crouch_speed: float = 5.5 # meter per second
@export var jump_impulse_velocity: float = 13.0
@export var air_acceleration: float = 60.0
@export var ground_acceleration: float = 45.0
@export var ground_friction: float = 40.0
@export var gravity_constant: float = 25.0
@export var max_run_velocity: float = 13.0
@export var max_walk_velocity: float = 6.0
@export var max_velocity: float = 30.0 # meter per second
@export var max_jump_velocity_from_still: float = 2.0

#-- Multiplayer Variables --#
var owned_by_peer_id = 0 # DON'T MAKE THIS ONREADY, this variable is assigned to BEFORE _ready

var parent_pivot_to_be_consumed = Transform3D()
var parent_pivot_is_consumed = true # default to true to mitigate consuming initial value

var camera_transform_to_be_consumed = Transform3D()
var camera_transform_is_consumed = true # default to true to mitigate consuming initial value

var velocity_to_be_consumed = Vector3()
var velocity_is_consumed = true

var transform_to_be_consumed = Transform3D()
var transform_is_consumed = true # default to true to mitigate consuming initial value

# action and movement inputs will always be "on" until it doesnt exist anymore
var movement_input_to_be_consumed = Vector3()
var actions_to_be_consumed: Array = []

###########################################################
# State Enum
###########################################################

# Uncomment when you want to start refactoring it into states
#signal player_state_changed(enum_state)
#signal gun_state_changed(enum_state)
#
#enum PlayerState {
#	WALKING,
#	RUNNING,
#	FALLING,
#	DEAD,
#	CROUCHING_WALK,
#	CROUCHING_RUN,
#}
#
#enum GunState {
#	SHOOTING,
#	SHOOTING_OUT_OF_BULLETS,
#	RELOADING,
#}

###########################################################
# Engine Callbacks
###########################################################

func _ready() -> void:
	State.set_state("debug_misc", str(multiplayer.get_unique_id()))
	if self.is_owned_by_me():
		camera.make_current()

		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		# height of the character from local origin + the crouch height delta with original height
		var body_height = body.shape.height
		headlimit_raycast.target_position = Vector3.UP * (body_height - crouch_height)

		# set all weapon to equipped
		for i in weapons.keys():
			if weapons[i]:
				weapons[i].set_to_player_object()
				weapons[i].player = self

		_on_IHealth_health_changed(i_health.current_health, i_health.current_armor)

		# For obvious reason, hide our own body.
		# If we are multiplayer, we need to check whether this is the playable character or not.
		player_model.hide()

		# duplicate this resource because the height is being changed every frame
		# if we don't do this, body.shape.height will retain its value even after restarting a level because it's a resource
		# after debugging and monitoring the resource count, this somehow doesn't leak memory
		var duplicated_body = body.shape.duplicate()
		body.shape = duplicated_body

	if not self.is_owned_by_me():
		pivot.hide()
		gui.hide()
		return


func _process(delta):
	if self.is_owned_by_me():
		var mouse_movement = LLInput.consume_mouse_input(Global.PROCESS_CONSUMER.PROCESS)
		handle_look(mouse_movement)
		handle_gun_sway(delta, mouse_movement)


func _physics_process(delta: float) -> void:
	apply_multiplayer_pull()

	# REGION - GET_INPUT
	var movement_input = Vector3()
	var actions = []

	if self.is_owned_by_me():
		movement_input = Player.get_movement_input(parent_pivot.global_transform.basis)
		actions = LLInput.get_player_actions()

	if not self.is_owned_by_me():
		movement_input = self.movement_input_to_be_consumed
		actions = self.actions_to_be_consumed
	# ENDREGION - GET_INPUT

	# REGION - HANDLE GENERAL THINGS
	for i in gun_container.get_children():
		i.global_transform = gun_container.global_transform

	if not disable_movement:
		handle_movement(movement_input, delta, actions)
		handle_crouching(delta, actions)

	handle_flash_dissipation(delta)
	handle_movement_sounds()

	if weapon.weapon_type != Global.WEAPON_TYPE.PROGRAMMED_TRIGGERED:
		handle_weapon_pickup(actions)
		handle_weapon_drop(actions)
		handle_weapon_reload(actions)
		handle_weapon_selection()
		fire_to_direction(delta, actions)

	Player.apply_shooting_knockback(self, camera, weapon)
	handle_airborne_monitoring()
	# ENDREGION - HANDLE GENERAL THINGS

	# LAST-ONLY HANDLING
	if self.is_owned_by_me():
		if Input.is_action_just_pressed("ui_cancel"):
			State.set_state("player_paused", !State.get_state("player_paused"))

		if Input.is_action_just_pressed("ui_end"): # DEBUG: SELF INFLICTED DAMAGE
			i_health.change_health_and_armor(-23)

		State.set_state("player_weapon_name", weapon.weapon_name)
		State.set_state("player_weapon_current_ammo", weapon.current_ammo)
		State.set_state("player_weapon_total_ammo", weapon.current_total_ammo)

		handle_multiplayer_push(movement_input, actions)


###########################################################
# Stateful function
###########################################################

### The body that's affected here is: Body size & position and Feet position only.
### When the body is reduced in height by N, it will go higher by N.
### The feet will follow the position of the body by getting higher N also.
### This process is reversible because of how crouching works which is:
###     * Crouch
###     * Come to normal
func handle_crouching(delta: float, actions: Array):
	# Handle pausing by zero-ing the input vector
	var is_paused = State.get_state("player_paused")
	var is_input_crouching = Global.PLAYER_INPUT_ACTIONS.CROUCH in actions

	if not is_paused and is_input_crouching or body.shape.height < body_original_height:
		is_crouching = true

	if not is_paused and not is_input_crouching and not headlimit_raycast.is_colliding():
		is_crouching = false

	if is_on_floor():
		if is_crouching:
			var crouch_delta = crouch_speed * delta
			var height_change = abs(crouch_height - body_original_height)
			var body_trans_target = body_original_local_translation + Vector3.UP * height_change / 2

			body.shape.height = move_toward(body.shape.height, crouch_height, crouch_delta)
			body.transform.origin = body.transform.origin.move_toward(body_trans_target, crouch_delta)
		else:
			var crouch_delta = crouch_speed * 1.5 * delta
			body.shape.height = move_toward(body.shape.height, body_original_height, crouch_delta)
			body.transform.origin = body.transform.origin.move_toward(body_original_local_translation, crouch_delta)

	else:
		if is_crouching:
			var height_change = abs(crouch_height - body_original_height)
			var body_trans_target = body_original_local_translation + Vector3.UP * height_change / 2

			body.shape.height = move_toward(body.shape.height, crouch_height, crouch_speed * delta)
			body.transform.origin = body.transform.origin.move_toward(body_trans_target, crouch_speed * delta)
		else:
			var crouch_delta = crouch_speed * 1.5 * delta
			body.shape.height = move_toward(body.shape.height, body_original_height, crouch_delta)
			body.transform.origin = body.transform.origin.move_toward(body_original_local_translation, crouch_delta)

	pivot.transform.origin = body.transform.origin + Vector3.UP * (body.shape.height / 2 - 0.35)
	headlimit_raycast.transform.origin = body.transform.origin + Vector3.UP * (body.shape.height / 2)


# TODO: decouple these codes into other smaller more concise function
func handle_movement(input_vector: Vector3, delta: float, actions: Array):
	# Handle pausing by zero-ing the input vector
	var is_paused = State.get_state("player_paused")
	var is_input_walk = Global.PLAYER_INPUT_ACTIONS.WALK in actions
	var is_input_jump_just_frame = Global.PLAYER_INPUT_ACTIONS.JUMP_JUST_FRAME in actions
	var is_input_jump = Global.PLAYER_INPUT_ACTIONS.JUMP in actions
	if is_paused:
		input_vector = Vector3.ZERO

	input_vector = input_vector.normalized()

	# vector used for snapping the character when checked the ground
	var snap_vector = 0.0

	if is_on_ceiling():
		desired_movement_velocity.y = 0

	# ON THE GROUND
	elif is_on_floor() and get_floor_normal().length() > 0.0:
		# slide the input_vector so that it would be checked the plane in which it walks
		var input_slanted = input_vector.slide(get_floor_normal()).normalized()
		snap_vector = 1.0

		# Player walk action that will decrease max_velocity
		if is_input_walk and is_crouching:
			current_max_movement_velocity = max_walk_velocity * 0.815
		elif is_crouching:
			current_max_movement_velocity = max_walk_velocity
		elif is_input_walk:
			current_max_movement_velocity = max_walk_velocity
		else:
			current_max_movement_velocity = max_run_velocity

		current_max_movement_velocity = current_max_movement_velocity * ((100.0 - weapon.max_velocity_penalty_percentage) / 100.0)

		# Apply Friction
		if input_slanted.length() > 0:
			var acceleration = ground_acceleration * ((100.0 - weapon.max_acceleration_penalty_percentage) / 100.0) * delta
			if desired_movement_velocity.normalized().dot(input_slanted.normalized()) < -0.98: # moving the opposite direction, counter strafing
				acceleration = acceleration * 2.5
			elif is_crouching:
				acceleration = acceleration * 0.5
			desired_movement_velocity = desired_movement_velocity.move_toward(input_slanted * current_max_movement_velocity, acceleration)
		else:
			desired_movement_velocity = Player.apply_friction(desired_movement_velocity, ground_friction, delta)

	# IN THE AIR
	else:
		snap_vector = 0.0

		# Temporarily disable velocity.y because we're about to calculate movement in the air
		# apart from the downward velocity of the player
		var current_gravity_velocity = desired_movement_velocity.y
		desired_movement_velocity.y = 0

		if input_vector.length() > 0:
			desired_movement_velocity = desired_movement_velocity.move_toward(input_vector * max_air_velocity, air_acceleration * delta)

		# Give back the original velocity.y after the air movement is calculated
		desired_movement_velocity.y = current_gravity_velocity

		desired_movement_velocity.y -= (gravity_constant * delta)
		current_max_movement_velocity = max_run_velocity

	# Jumping mechanics, affects desired_movement_velocity. this is because
	# the jumping height could be affected by the movement velocity, which is
	# affected by the angle of the surface.
	#
	# EXAMPLE: if the desired velocity isn't changed, player who went up a slope
	#          will have higher jumping velocity than the one going downwards.
	if not is_paused and is_on_floor() and (is_input_jump_just_frame or (auto_bhop and is_input_jump)):
		snap_vector = 0.0

		var no_y = Vector3(desired_movement_velocity)
		no_y.y = 0.0

		max_air_velocity = max(no_y.length(), max_jump_velocity_from_still)
		desired_movement_velocity = Util.clamp_vector3(input_vector * current_max_movement_velocity, desired_movement_velocity.length())
		desired_movement_velocity.y = jump_impulse_velocity

	# clamp the velocity and gravity to max speed
	desired_movement_velocity = Util.clamp_vector3_y_axis(desired_movement_velocity, max_velocity)

	# apply to is_mouse_moving_time and slide
	# TODO: fix snapping to sides of walls when stepping fast away from it
	self.set_velocity(desired_movement_velocity)
	self.floor_snap_length = snap_vector

	if self.get_floor_normal().length() > 0:
		self.set_up_direction(self.get_floor_normal())
	else:
		self.set_up_direction(Vector3.UP)

	self.set_floor_stop_on_slope_enabled(true)
	self.set_max_slides(4)
	self.set_floor_max_angle(0.785398)
	self.move_and_slide()
	desired_movement_velocity = self.velocity

	State.set_state("player_velocity_length", desired_movement_velocity.length())


func handle_look(mouse_movement: Vector2) -> void:
	if self.is_owned_by_me() and !State.get_state("player_paused"):
		# right and left
		pivot.rotate_x(-mouse_movement.y)
		pivot.rotation.x = clamp(pivot.rotation.x, -PI/2 + 0.01, PI/2 - 0.01)

		# up and down
		parent_pivot.rotate_y(-mouse_movement.x)


func hide_all_weapon() -> void:
	for w in weapons.values():
		if w:
			w.hide()


func handle_weapon_selection() -> void:
	var is_paused = State.get_state("player_paused")

	if not is_paused:
		if Input.is_action_just_pressed("player_weapon_swap"):
			_switch_weapon_routine(last_weapon_slot_used)
		if Input.is_action_just_pressed("player_weapon_gun_primary"):
			_switch_weapon_routine(Global.WEAPON_SLOT.PRIMARY)
		if Input.is_action_just_pressed("player_weapon_gun_secondary"):
			_switch_weapon_routine(Global.WEAPON_SLOT.SECONDARY)
		if Input.is_action_just_pressed("player_weapon_gun_knife"):
			_switch_weapon_routine(Global.WEAPON_SLOT.MELEE)
		if Input.is_action_just_pressed("player_cycle_grenade"):
			_switch_weapon_routine(Global.WEAPON_SLOT.UTILITY)


func fire_to_direction(delta: float, actions: Array) -> void:
	var is_paused = State.get_state("player_paused")

	# FIRST TRIGGER
	if not is_paused and Global.PLAYER_INPUT_ACTIONS.SHOOT_1_PRESSED in actions and weapon.can_shoot() and weapon.trigger_on(delta):
		weapon.first_activate(pivot)

	if not is_paused and Global.PLAYER_INPUT_ACTIONS.SHOOT_1_RELEASED in actions:
		weapon.trigger_off()


	# SECOND TRIGGER
	# TODO: make shooting routine to have many modes
	if not is_paused and Global.PLAYER_INPUT_ACTIONS.SHOOT_2_PRESSED in actions and weapon.can_shoot() and weapon.second_trigger_on():
		pass

	if not is_paused and Global.PLAYER_INPUT_ACTIONS.SHOOT_2_RELEASED in actions:
		weapon.second_trigger_off()


func handle_weapon_drop(actions: Array) -> void:
	if not disable_weapon_drop and Global.PLAYER_INPUT_ACTIONS.DROP_WEAPON in actions and weapon.weapon_slot != Global.WEAPON_SLOT.MELEE:
		_drop_weapon(weapon)


# Unlike drop_weapon, this one will only remove_at it from the inventory and not add it to the world
func remove_weapon(weapon_slot):
	var w = weapons.get(weapon_slot)
	if w:
		# remove_at from child
		gun_container.remove_child(w)

		# throw it into the world (for now just place it checked the ground from where the camera origin is)
		Util.add_to_world(w)

		# make set_to_world_object
		w.set_to_world_object()

		# make the weapons (dict) currenly equipped to null
		if weapons[w.weapon_slot] == w:
			weapons[w.weapon_slot] = null

		if weapon == w:
			weapon = null

		# switch to the currently used weapon
		_switch_weapon_routine(current_weapon_slot)


# TODO: add weapon pickup when near
func handle_weapon_pickup(actions: Array) -> void:
	if Global.PLAYER_INPUT_ACTIONS.INTERACT in actions:
		var from = camera.global_transform.origin
		var to = from + -camera.global_transform.basis.z * 5.0

		# get the weapon being looked at
		var space_state = camera.get_world_3d().direct_space_state
		var query = PhysicsRayQueryParameters3D.create(from, to, Global.PHYSICS_LAYERS.GUN, [self])
		var ray_result = space_state.intersect_ray(query)
		var colliding = ray_result.get("collider")

		if colliding and colliding is GenericWeapon:
			_weapon_pickup_routine(colliding)


func _weapon_pickup_routine(w: GenericWeapon) -> void:
	# set the weapon to the one chosen by the player
	var existing_weapon = _set_weapon(w)

	# if player already have a weapon in the chosen weapon slot
	if existing_weapon:
		_drop_weapon(existing_weapon)


func replace_weapon_and_free_existing(w: GenericWeapon) -> void:
	var existing_weapon = _set_weapon(w)

	if existing_weapon:
		existing_weapon.queue_free()


func handle_weapon_reload(actions: Array) -> void:
	var is_paused = State.get_state("player_paused")
	if not is_paused and Global.PLAYER_INPUT_ACTIONS.RELOAD in actions and weapon.can_reload():
		State.set_state("player_reloaded", Engine.get_physics_frames())
		weapon.reload_trigger()


# Drop the given weapon and then return whether the weapon is dropped or not.
# Return true only when the weapon is dropped and false if the weapon can't be dropped.
func _drop_weapon(w: GenericWeapon) -> void:
	w.player = null
	w.unequip()
	w.show()

	# remove_at from child
	gun_container.remove_child(w)

	# throw it into the world (for now just place it checked the ground from where the camera origin is)
	for i in get_tree().root.get_children():
		if i is Node3D:
			i.add_child(w)

	# make set_to_world_object
	w.set_to_world_object()

	randomize()
	w.global_transform.origin = camera.global_transform.origin
	w.global_rotate(Vector3(randf_range(-1, 1), randf_range(-1, 1), randf_range(-1, 1)).normalized(), PI * randf_range(0, 2))

	# add force to the gun
	w.apply_central_impulse(-camera.global_transform.basis.z * (15 + desired_movement_velocity.length() / 2))
	w.apply_torque_impulse(-camera.global_transform.basis.z.rotated(Vector3.UP, randf_range(-PI/2, PI/2)) * randf_range(0.1,0.5))

	# make the weapons (dict) currenly equipped to null
	if weapons[w.weapon_slot] == w:
		weapons[w.weapon_slot] = null

	if weapon == w:
		weapon = null

	# switch to the currently used weapon
	_switch_weapon_routine(current_weapon_slot)


# Set the given weapon to the inventory then return an already existing weapon.
# If player doesn't have weapon already, then return null.
func _set_weapon(w: GenericWeapon) -> GenericWeapon:
	w.player = self
	w.equip()

	var existing_weapon = weapons[w.weapon_slot]

	var parent = w.get_parent()
	if parent:
		parent.remove_child(w)

	weapons[w.weapon_slot] = w

	# Add to gun container
	gun_container.add_child(w)
	w.global_transform = gun_container.get_global_transform()

	# Set the weapon to be equipped
	w.set_to_player_object()

	# TODO make an option for auto switch checked pickup
	# Hide it at pickup
	w.hide()

	return existing_weapon


func _switch_weapon_routine(weapon_slot) -> void:
	hide_all_weapon()
	if weapon:
		weapon.unequip()

	if weapons[weapon_slot]:
		if current_weapon_slot != weapon_slot:
			last_weapon_slot_used = current_weapon_slot
			current_weapon_slot = weapon_slot

		weapon = weapons[weapon_slot]
	else:
		for k in weapons.keys():
			if weapons[k]:
				weapon = weapons[k]
				current_weapon_slot = weapon.weapon_slot
				last_weapon_slot_used = weapon.weapon_slot

	weapon.show()
	weapon.equip()


# TODO: an effect where the player's sight is "nudged" when hit checked the head
func handle_aim_punch() -> void:
	pass


func handle_gun_sway(delta: float, mouse_movement: Vector2) -> void:
	_handle_turning_sway(delta, mouse_movement)
	_handle_movement_sway(delta)


# Will turn the gun left and right based checked the head turn
# Modified version of this code https://godotengine.org/qa/68649/how-to-make-the-sway-effect-checked-weapons-of-fps-3d-games
func _handle_turning_sway(delta: float, mouse_movement: Vector2) -> void:
	if mouse_movement.length() > 0:
		var sway_movement = Vector2(clamp(mouse_movement.y, -mouse_turn_max_sensitivity, mouse_turn_max_sensitivity), clamp(mouse_movement.x, -mouse_turn_max_sensitivity, mouse_turn_max_sensitivity))

		# Turning Up-Down
		var max_turn_up_down = 1
		var raw_next_x_rotation = move_toward(gun_container.rotation_degrees.x, gun_container.rotation_degrees.x - (sway_movement.x), delta * 5)
		gun_container.rotation_degrees.x = clamp(raw_next_x_rotation, -max_turn_up_down, max_turn_up_down)

		# Turning Right-Left
		var max_turn_right_degree = 2.5
		var raw_next_y_rotation = move_toward(gun_container.rotation_degrees.y, gun_container.rotation_degrees.y - (sway_movement.y), delta * 12)
		gun_container.rotation_degrees.y = clamp(raw_next_y_rotation, -max_turn_right_degree, max_turn_right_degree)
	else:
		# Recovery from moving to original rotation
		gun_container.rotation_degrees.x = move_toward(gun_container.rotation_degrees.x, gun_container_original_rotation.x, delta * 5)
		gun_container.rotation_degrees.y = move_toward(gun_container.rotation_degrees.y, gun_container_original_rotation.y, delta * 12)


func _handle_movement_sway(delta) -> void:
	var movement_ratio = min(desired_movement_velocity.length() / max_run_velocity, 1.0)

	var farthest_backward_sway = 0.08 * movement_ratio
	var backward_sway_destination = move_toward(gun_container.transform.origin.z, gun_container_original_transform.origin.z + farthest_backward_sway, delta)
	gun_container.transform.origin.z = backward_sway_destination

	var farthest_down_sway = -0.01 * movement_ratio
	var down_sway_destination = move_toward(gun_container.transform.origin.y, gun_container_original_transform.origin.y + farthest_down_sway, delta)
	gun_container.transform.origin.y = down_sway_destination


func handle_flash_dissipation(delta) -> void:
	var flash_color = Color(DEFAULT_FLASH_COLOR)
	flash_color.a = clamp(flash_remaining_second, 0, 1)
	flash_overlay.color = flash_color
	flash_remaining_second = clamp(flash_remaining_second - delta, 0, FLASH_MAXIMUM_DURATION)


func _get_was_airborne() -> bool:
	return (last_time_on_ground < Engine.get_physics_frames()) and is_on_floor()


func handle_airborne_monitoring() -> void:
	var y_position = global_transform.origin.y
	if is_on_floor():
		last_time_on_ground = Engine.get_physics_frames()
		current_max_airborne_height = y_position
	else:
		current_max_airborne_height = max(y_position, current_max_airborne_height)


# BUG: What happened when the player is using a heavy weapon where current_max_movement_velocity will always be below `max_run_velocity - 1`?
func handle_movement_sounds() -> void:
	var was_airborne = _get_was_airborne()
	var fall_height = current_max_airborne_height - global_transform.origin.y

	if is_on_floor() and was_airborne and fall_height > DEFAULT_AUDIBLE_FALL_HEIGHT:
		dropping_audio_player.play()
	elif is_on_floor() and desired_movement_velocity.length() > max_run_velocity - 6 and !walking_audio_player.is_playing(): # BUG: make step rythm according to how fast desired_movement_velocity is
		walking_audio_player.play()


#-- multiplayer --#


# NOTE ABOUT physics_tick:
#     - In server side, physics_tick is latest server tick used by client.
#     - In client side, physics_tick is closest tick client has to compare with what the server has, for reconciliation
func set_multiplayer_pull(physics_tick: int, movement_input: Vector3, actions: Array, camera_transform: Transform3D, parent_pivot_transform: Transform3D, next_weapon_slot: int, client_trust_data: Dictionary) -> void:
	if not multiplayer.is_server():
		if not self.is_owned_by_me():
			self.transform_to_be_consumed = client_trust_data.next_global_transform
			self.transform_is_consumed = false

			self.actions_to_be_consumed = actions

			self.camera_transform_to_be_consumed = camera_transform
			self.camera_transform_is_consumed = false

			self.parent_pivot_to_be_consumed = parent_pivot_transform
			self.parent_pivot_is_consumed = false

		# TODO: there might be a chance where physics_tick will not be compared below, so on the frame where it can be compared, compare it.
		# if self.is_owned_by_me() and physics_tick > 0 and physics_tick < Engine.get_physics_frames():
		if self.is_owned_by_me() and physics_tick > 0 and physics_tick <= Engine.get_physics_frames():
			var past_client_data = GameMode.get_timeline_snapshot(physics_tick)
			if past_client_data != {}:
				# check if the discrepancies between client and server is large
				var past_transform = past_client_data[multiplayer.get_unique_id()]["global_transform"]
				var distance = client_trust_data.next_global_transform.origin.distance_to(past_transform.origin)

				# fix client origin based on what server sees
				# TODO #153: prediction error happens because there needs to be a rollback in server for movement
				#            this make sure the rate of input is determined by physics_tick instead of the latency
				if distance > 0.0:
					self.transform_to_be_consumed = client_trust_data.next_global_transform
					self.transform_is_consumed = false

	if multiplayer.is_server() and not self.is_owned_by_me():
		self.movement_input_to_be_consumed = movement_input
		self.actions_to_be_consumed = actions

		self.camera_transform_to_be_consumed = camera_transform
		self.camera_transform_is_consumed = false

		self.parent_pivot_to_be_consumed = parent_pivot_transform
		self.parent_pivot_is_consumed = false

		# we trust the client just a bit, at least the server frame it's using
		# if physics_tick <= Engine.get_physics_frames() and (Engine.get_physics_frames() - physics_tick) < Global.NETWORK_MAX_DELAY_TOLERANCE:
		# 	var timeline_data = GameMode.get_timeline_snapshot(physics_tick)
		# 	if timeline_data != {}:
		# 		var past_player_data = timeline_data[self.owned_by_peer_id]

		# 		self.transform_to_be_consumed = past_player_data["global_transform"]
		# 		self.transform_is_consumed = false

		# 		self.velocity_to_be_consumed = past_player_data["velocity"]
		# 		self.velocity_is_consumed = false


# only used by the server
func get_multiplayer_pull() -> Array:
	return [
		owned_by_peer_id,
		Time.get_unix_time_from_system(),
		self.movement_input_to_be_consumed,
		self.actions_to_be_consumed,
		camera.global_transform,
		parent_pivot.global_transform,
		current_weapon_slot,
		GameMode.build_client_trust_data(self.global_transform),
	]


# this will use the value cached from multiplayer server and synced the entities all over the place
func apply_multiplayer_pull() -> void:
	if multiplayer.has_multiplayer_peer() and not multiplayer.is_server():
		if not self.transform_is_consumed:
			self.global_transform = self.transform_to_be_consumed # TODO: use lerp in the future
			self.transform_is_consumed = true

		# if not self.velocity_is_consumed:
		# 	self.velocity = self.velocity_to_be_consumed
		# 	self.desired_movement_velocity = self.velocity_to_be_consumed
		# 	self.velocity_is_consumed = true

	# BUG: turn this into BASIS instead so origin couldnt be manipulated by client
	if not self.is_owned_by_me():
		if not self.parent_pivot_is_consumed:
			self.parent_pivot.global_transform = self.parent_pivot_to_be_consumed
			self.parent_pivot_is_consumed = true

		if not self.camera_transform_is_consumed:
			self.camera.global_transform = self.camera_transform_to_be_consumed
			self.camera_transform_is_consumed = true


func handle_multiplayer_push(movement_input: Vector3, actions: Array) -> void:
	var camera_transform = self.camera.global_transform
	var parent_pivot_transform = self.parent_pivot.global_transform
	var current_weapon_slot_slot = self.current_weapon_slot
	var server_physics_tick = GameMode.multiplayer_latest_server_physics_tick

	if multiplayer.has_multiplayer_peer() and not multiplayer.is_server():
		GameMode.push_sync_player_entity.rpc(server_physics_tick, movement_input, actions, camera_transform, parent_pivot_transform, current_weapon_slot_slot, GameMode.build_client_trust_data(self.global_transform))


###########################################################
# Static Function
###########################################################

# TODO: use the player variable and knock the head upwards when getting headshot
static func apply_shooting_knockback(_player, c: CameraSmoothPhysics, w: GenericWeapon):
	var spr_inacc = w.get_knockback_inaccuracy()

	# If rotated by a positive number, it goes to the right, vice versa
	c.rotated_angle_horizontal = spr_inacc[0]

	# If rotated by a positive number, it goes to the top, vice versa
	c.rotated_angle_vertical = spr_inacc[1]


###########################################################
# Stateless Function
###########################################################

static func get_movement_input(camera_basis: Basis) -> Vector3:
	var input_direction = Vector3.ZERO
	if LLInput.is_action_pressed("player_forward"):
		input_direction += -camera_basis.z
	if LLInput.is_action_pressed("player_backward"):
		input_direction += camera_basis.z
	if LLInput.is_action_pressed("player_right"):
		input_direction += camera_basis.x
	if LLInput.is_action_pressed("player_left"):
		input_direction += -camera_basis.x

	input_direction.y = 0
	return input_direction.normalized()

static func apply_friction(new_velocity: Vector3, friction_constant: float, delta: float) -> Vector3:
	if new_velocity.length() <= friction_constant * delta:
		return Vector3()
	else:
		var friction_vector = new_velocity.normalized() * friction_constant * delta
		return new_velocity - friction_vector


func is_owned_by_me() -> bool:
	var is_owned_by_peer = self.multiplayer.has_multiplayer_peer() and owned_by_peer_id == self.multiplayer.get_unique_id()
	var is_single_player = not self.multiplayer.has_multiplayer_peer() and owned_by_peer_id == 0

	return is_owned_by_peer or is_single_player


###########################################################
# Signal Function
###########################################################


func _on_Area_body_entered(w):
	if w is GenericWeapon:
		if not weapons.get(w.weapon_slot) and w.is_auto_pickupable():
			_weapon_pickup_routine(w)


func _on_IHealth_health_changed(current_health: float, current_armor: float):
	State.set_state("player_health",  current_health)
	State.set_state("player_armor",  current_armor)


func _on_IHealth_dead():
	# TODO: add a routine to make the person dead and then respawn again
	pass


func _on_IFlash_flashed(flash_position: Vector3):
	var distance = clamp(camera.global_transform.origin.distance_to(flash_position), 0, FLASH_MAXIMUM_DISTANCE)
	var distance_ratio = remap(distance, 0, FLASH_MAXIMUM_DISTANCE, 1, 0)

	var forward = -camera.global_transform.basis.z.normalized()
	var to_other = (flash_position - self.global_transform.origin).normalized()
	var angle = clamp(forward.angle_to(to_other), 0, FLASH_MAXIMUM_ANGLE)
	var angle_ratio = remap(angle, 0, FLASH_MAXIMUM_ANGLE, 1, 0.1)

	var calc_flash_duration = FLASH_MAXIMUM_DURATION * distance_ratio * angle_ratio
	flash_remaining_second = clamp(calc_flash_duration, 0, FLASH_MAXIMUM_DURATION)


func _on_ISmokeScreen_vision_smoked(intensity: float) -> void:
	var smoke_screen_color = Color(DEFAULT_SMOKED_COLOR)
	smoke_screen_color.a = clamp(intensity, 0, 1)
	smoke_overlay.color = smoke_screen_color
