extends HBoxContainer

func set_row_values(number: String, create_date_time: String, completion_time: String, accuracy_percentage: String, target_count: String) -> void:
	$NumberLabel.text = number
	$CreatedDateTimeLabel.text = create_date_time
	$CompletionTimeLabel.text = completion_time
	$AccuracyPercentageLabel.text = accuracy_percentage
	$TargetCountLabel.text = target_count