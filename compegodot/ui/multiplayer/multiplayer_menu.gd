extends PanelContainer

@onready var lobby_initial_menu = self.get_node("%LobbyInitialMenu")
@onready var lobby_menu = self.get_node("%LobbyMenu")
@onready var all_menu = [
	self.lobby_initial_menu,
	self.lobby_menu
]

# Called when the node enters the scene tree for the first time.
func _ready():
	State.connect("state_multiplayer_lobby_is_entered", _on_state_multiplayer_lobby_is_entered)
	self._on_state_multiplayer_lobby_is_entered(State.get_state("multiplayer_lobby_is_entered"))


func hide_all_menu_and_show_one(shown_menu: CanvasItem) -> void:
	for menu in self.all_menu:
		menu.hide()

	shown_menu.show()


func _on_state_multiplayer_lobby_is_entered(value: bool) -> void:
	if value:
		hide_all_menu_and_show_one(self.lobby_menu)

	if not value:
		hide_all_menu_and_show_one(self.lobby_initial_menu)
