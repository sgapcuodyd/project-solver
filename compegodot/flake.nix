{
  # main
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    # nixgl.url = "github:guibou/nixGL";
  };

  # dev
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
      # , nixgl
    }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (
      system:
      let
        pkgs = import nixpkgs {
          inherit system;
          # overlays = [ nixgl.overlay ];
          # config.allowUnfree = true;
        };
        # customGodot = pkgs.callPackage ./nix/godot4.nix { };
      in
      {
        devShells.default = pkgs.mkShell {
          packages =
            [
              # customGodot
              pkgs.godot_4
              # pkgs.nixgl.auto.nixGLDefault # must be ran with --impure
            ];

          # Alias the godot engine to use nixGL
          # shellHook = ''
          #   alias justgodot="${customGodot}/bin/godot"
          #   alias godot="nixGL godot"
          # '';
        };
      }
    );
}
