extends GenericWeapon
class_name Grenade

const STAYING_STILL_DURATION_SECOND = 1.0
const STAYING_STILL_LINEAR_VELOCITY_LENGTH = 0.1
const STAYING_STILL_MINIMAL_DISTANCE_FROM_GROUND = 0.01

@export var nade_fuse_time = 2.0
@export var nade_destroyed_time = 1000.0 # how long before the nade destroys itself after being deployed
@export var explosion_scene: PackedScene = null # TODO: probably create another child class for FragGrenade?
@export var explosion_sound_scene: PackedScene = null # TODO: probably create another child class for FragGrenade?
@export var smoke_effect_scene: PackedScene = null # TODO: probably create another child class for SmokeGrenade?
@export var influence_area_path: NodePath = ""

var influence_area = null
var influence_area_range = 0
var activate_world_sensors = false
var grenade_shape_radius = 0.0
var _nade_destroyed_timer_count_second = 0.0
var _nade_staying_still_count_second = 0.0

func _get_configuration_warnings():
	var parent_config_warning = super._get_configuration_warnings()
	if parent_config_warning.size() > 0:
		return parent_config_warning

	if not _is_influence_area():
		return ["Set influence_area_path to valid Area3D"]

	if self.weapon_type == Global.WEAPON_TYPE.SMOKE_GRENADE and self.smoke_effect_scene == null:
		return ["Set Smoke Effect Scene for smoke grenade"]
	return []


func _ready():
	super._ready()
	if !Engine.is_editor_hint():
		Util.handle_err(anim_player.connect("animation_finished",Callable(self,"_on_AnimationPlayer_animation_finished")))

		# INIT Influence Area3D
		influence_area = get_node(influence_area_path)
		influence_area_range = ((influence_area.get_child(0)).shape as SphereShape3D).radius
		grenade_shape_radius = $CollisionShape3D.get_shape().radius


func _physics_process(delta):
	if weapon_type == Global.WEAPON_TYPE.SMOKE_GRENADE and activate_world_sensors:
		# sensors for staying still checked the ground
		self._sensor_staying_still(delta)

		# sensors for timing if grenade lived for too long
		self._sensor_nade_destroyed_timer(delta)


func _active_grenade_routine():
	if weapon_type == Global.WEAPON_TYPE.FRAG_GRENADE:
		await get_tree().create_timer(nade_fuse_time).timeout

		var explosion_sound = explosion_sound_scene.instantiate()
		Util.add_to_world(explosion_sound)
		explosion_sound.global_transform.origin = global_transform.origin
		explosion_sound.custom_play()

		var ray_results = _get_ray_results_of_frag_influenced_objects()
		for r in ray_results:
			_apply_damage(r)
			_apply_impulse(r)

		var explosion_effect_size = 20
		var explosion_effect_node = explosion_scene.instantiate()
		var translate_then_scale_xform = Transform3D().scaled(Vector3.ONE * explosion_effect_size)
		translate_then_scale_xform.origin = global_transform.origin

		explosion_effect_node.global_transform = translate_then_scale_xform
		explosion_effect_node.emitting = true
		Util.add_to_world(explosion_effect_node)

		queue_free()
	if weapon_type == Global.WEAPON_TYPE.FLASH_GRENADE:
		await get_tree().create_timer(nade_fuse_time).timeout

		var explosion_sound = explosion_sound_scene.instantiate()
		Util.add_to_world(explosion_sound)
		explosion_sound.global_transform.origin = global_transform.origin

		explosion_sound.custom_play()

		var ray_results = _get_ray_results_of_flash_influenced_players()
		for r in ray_results:
			_apply_flashbang(r)

		queue_free()
	if weapon_type == Global.WEAPON_TYPE.SMOKE_GRENADE:
		# most operations for smoke grenade is done through physics update
		activate_world_sensors = true

# Will give an array of ray_result back
func _get_ray_results_of_frag_influenced_objects() -> Array:
	var overlapping_bodies = influence_area.get_overlapping_bodies()
	var result = []

	# For rays
	var space_state = get_world_3d().direct_space_state
	var from = global_transform.origin
	var shootable_collision_mask = Global.PHYSICS_LAYERS.WORLD | Global.PHYSICS_LAYERS.GUN | Global.PHYSICS_LAYERS.TEAM_1 | Global.PHYSICS_LAYERS.TEAM_2

	# Filtering overlapping bodies
	# TODO-BUG #77: Grenade can't damage player if it's inside the player
	for i in overlapping_bodies:
		# 1. Check whether grenade has line of sight to object
		var to = i.global_transform.origin
		var query = PhysicsRayQueryParameters3D.create(from, to, shootable_collision_mask, [self])
		var ray_result = space_state.intersect_ray(query)
		var colliding = ray_result.get("collider")

		# 2. Add to result if it does pass the check
		if colliding and colliding == i and (colliding is RigidBody3D or "i_health" in i or "i_interact" in i):
			result.append(ray_result)

	return result


# Will give an array of ray_result back
func _get_ray_results_of_flash_influenced_players() -> Array:
	var overlapping_bodies = get_tree().get_nodes_in_group(Global.SPAWN_TYPE_GROUP_TEAM_1)
	overlapping_bodies.append_array(get_tree().get_nodes_in_group(Global.SPAWN_TYPE_GROUP_TEAM_2))
	overlapping_bodies.append_array(get_tree().get_nodes_in_group(Global.SPAWN_TYPE_GROUP_UNIVERSAL))

	var result = []

	# For rays
	var space_state = get_world_3d().direct_space_state
	var from = global_transform.origin
	var flashable_collision_mask = Global.PHYSICS_LAYERS.TEAM_1 | Global.PHYSICS_LAYERS.TEAM_2 | Global.PHYSICS_LAYERS.WORLD

	# Filtering overlapping bodies
	# TODO-BUG #77: Grenade can't flash player if it's inside the player
	for i in overlapping_bodies:
		# 1. Check whether grenade has line of sight to object
		var to = i.global_transform.origin
		var query = PhysicsRayQueryParameters3D.create(from, to, flashable_collision_mask, [self])
		var ray_result = space_state.intersect_ray(query)
		var colliding = ray_result.get("collider")

		# 2. Add to result if it does pass the check
		if colliding and colliding == i and ("i_flash" in i):
			result.append(ray_result)

	return result


# apply damage based checked distance linearly instead of squared because screw physics
func _apply_damage(ray_result: Dictionary):
	var collider = ray_result.get("collider")
	var ray_position = ray_result.get("position", Vector3())

	if collider and "i_health" in collider:
		# Math stuff for area of influence modifier (the farther the smaller the damage)
		var distance = global_transform.origin.distance_to(ray_position)
		var influence_range_ratio = (influence_area_range - distance) / influence_area_range
		var modifier = max(influence_range_ratio, 0)
		var modified_damage = -base_damage * modifier

		collider.i_health.change_health_and_armor(modified_damage)


func _apply_impulse(ray_result: Dictionary):
	var collider = ray_result.get("collider")
	var ray_position = ray_result.get("position", Vector3())

	if collider is RigidBody3D:
		# Math stuff for area of influence modifier (the farther the smaller the damage)
		var distance = global_transform.origin.distance_to(ray_position)
		var influence_range_ratio = (influence_area_range - distance) / influence_area_range
		var modifier = max(influence_range_ratio, 0)
		var imp_direction = (ray_position - global_transform.origin).normalized()
		var modified_impulse_power = imp_direction * modifier * 25

		collider.apply_impulse(modified_impulse_power, ray_position)


func _apply_flashbang(ray_result: Dictionary):
	var collider = ray_result.get("collider")

	if "i_flash" in collider: # TODO: create i_flash and apply to player
		collider.i_flash.flash(self.global_transform.origin)


func _is_influence_area() -> bool:
	var test = get_node(influence_area_path)
	return test and (test is Area3D)


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == anim_shoot_name or anim_name == anim_shoot_secondary_name:
		influence_area.monitoring = true

		# Cache it for a second
		var p = player
		var camera = p.camera

		# Set to world
		p.remove_weapon(weapon_slot)
		self.collision_layer = 0
		self.collision_mask = Global.PHYSICS_LAYERS.WORLD
		self.global_transform.origin = p.pivot.global_transform.origin
		self.add_collision_exception_with(p)

		# Throw it from the front of the player
		global_transform.origin = camera.global_transform.origin
		global_rotate(Vector3(randf_range(-1, 1), randf_range(-1, 1), randf_range(-1, 1)).normalized(), PI * randf_range(0, 2))

		# Throwing power based checked animation name (because it determine between first and secondary shot)
		var throwing_power = 18
		if anim_name == anim_shoot_secondary_name:
			throwing_power = 7

		# Add force to the gun
		apply_central_impulse(-camera.global_transform.basis.z * throwing_power + (p.desired_movement_velocity))
		apply_torque_impulse(Vector3(randf_range(-1, 1), randf_range(-1, 1), randf_range(-1, 1)).normalized() * 0.10)

		# Depends checked the types of grenade
		_active_grenade_routine()

func _sensor_nade_destroyed_timer(delta: float) -> void:
	self._nade_destroyed_timer_count_second += delta
	if self._nade_destroyed_timer_count_second > self.nade_destroyed_time:
		queue_free()

func get_is_touching_ground() -> bool: # TODO: this will break in Godot 4, good luck.
	var space_state = get_world_3d().direct_space_state
	var current_position = self.global_transform.origin
	var target_position = self.global_transform.origin + (Vector3.DOWN * (STAYING_STILL_MINIMAL_DISTANCE_FROM_GROUND + grenade_shape_radius))
	var query = PhysicsRayQueryParameters3D.create(current_position, target_position, Global.PHYSICS_LAYERS.WORLD, [self])
	var result = space_state.intersect_ray(query)

	return !result.is_empty()

func _sensor_staying_still(delta: float) -> void:
	if !Engine.is_in_physics_frame():
		printerr("_sensor_staying_still need to be in physics frame to work, dummy.")
		return

	var is_touching_ground = self.get_is_touching_ground()
	if !is_touching_ground:
		_nade_staying_still_count_second = 0
		return

	if is_touching_ground:
		_nade_staying_still_count_second += delta

	# NOTE: There's a need to check for grenade type here because _smoke_grenade_activation are only used by smoke grenade (duh).
	#       We can use signal, but _smoke_grenade_activation probably need to yield for physics update anyway.
	#       So we call _smoke_grenade_activation directly instead, because _sensor_staying_still should only be called in _physics_process.
	if weapon_type == Global.WEAPON_TYPE.SMOKE_GRENADE:
		var is_ground_long_enough = _nade_staying_still_count_second > STAYING_STILL_DURATION_SECOND
		var is_staying_still = self.linear_velocity.length() <= STAYING_STILL_LINEAR_VELOCITY_LENGTH

		if is_ground_long_enough and is_staying_still:
			self._smoke_grenade_activation()

func _smoke_grenade_activation() -> void: # TODO
	# DEBUG: sounds for debug, to know whether its working in game or not
	var smoke_effect_node = smoke_effect_scene.instantiate()
	Util.add_to_world(smoke_effect_node)
	smoke_effect_node.global_transform.origin = global_transform.origin

	var explosion_sound = explosion_sound_scene.instantiate()
	Util.add_to_world(explosion_sound)
	explosion_sound.global_transform.origin = global_transform.origin

	explosion_sound.custom_play()
	queue_free()
