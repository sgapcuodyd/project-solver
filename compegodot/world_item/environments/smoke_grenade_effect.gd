extends Node3D

@export var duration_second = 20.0
@export var smoke_transition_eased = 0.0 # (float, EASE)

var smoke_mesh = null
var area_of_effect = null
var is_shrinking = false
var _original_radius = 0.0
var _original_scale = Vector3.ZERO
var _expansion_rate = Vector3.ONE * 10

func _ready():
	smoke_mesh = $SmokeMesh
	area_of_effect = $Area3D

	_original_radius = smoke_mesh.mesh.radius
	_original_scale = smoke_mesh.scale

	smoke_mesh.scale = Vector3.ZERO

	var timer = get_tree().create_timer(duration_second)
	timer.connect("timeout",Callable(self,"_on_timer_timeout"))


func _physics_process(delta):
	if smoke_mesh.scale.length() < _original_scale.length() and not is_shrinking:
		smoke_mesh.scale = smoke_mesh.scale + (delta * _expansion_rate)

	if smoke_mesh.scale.length() >= 0 and is_shrinking:
		smoke_mesh.scale = smoke_mesh.scale - (delta * _expansion_rate)

	if smoke_mesh.scale.length() >= _original_scale.length():
		smoke_mesh.scale = _original_scale

	if smoke_mesh.scale.length() <= 0.1 and is_shrinking:
		queue_free()

	smoke_mesh.rotate_y(delta * 1)
	smoke_mesh.rotate_x(delta * 1)

	# this might look dumb, but believe me, not that dumb.
	for body in area_of_effect.get_overlapping_bodies():
		if "i_smoke_screen" in body:
			var smoke_middle = Vector3(body.global_transform.origin)
			smoke_middle.y = smoke_middle.y - 2 # the height of the player

			var distance = global_transform.origin.distance_to(smoke_middle)
			var ratio = (_original_radius - distance)/_original_radius
			body.i_smoke_screen.set_vision_intensity(ease(ratio, smoke_transition_eased))

			if smoke_mesh.scale.length() <= 0.1 and is_shrinking:
				body.i_smoke_screen.reset_vision_intensity()


func _on_timer_timeout():
	is_shrinking = true
