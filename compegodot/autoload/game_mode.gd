extends Node

var player_scene = preload("res://player/player.tscn")
var player_group_names = [Global.SPAWN_TYPE_GROUP_UNIVERSAL, Global.SPAWN_TYPE_GROUP_TEAM_1, Global.SPAWN_TYPE_GROUP_TEAM_2]

# array of dictionary of player state data
# for clients I think filling this with other player's data is unnecessary
# the structure are as followed
# 	{
# 		peer_id: DEFAULT_PLAYER_SNAPSHOT
# 	}
var player_snapshot_timeline: Array[Dictionary] = []
const DEFAULT_TIMELINE_SIZE =  75 * 1    # 75 physics frame per second with 10 seconds of rollback
const DEFAULT_PLAYER_SNAPSHOT = {         # TODO: also record animation state so we can rollback the position of things too
	"global_transform": Transform3D(),
	"velocity": Vector3(),
}

# only used by client for prediction, DON'T USE SERVER SIDE use Engine.get_physics_frames() instead
var multiplayer_latest_client_physics_tick_when_server_info_comes = 0
var multiplayer_latest_server_physics_tick = 0

func _ready():
	Util.handle_err(get_tree().connect("node_added",Callable(self,"_on_tree_node_added")))

	# init timeline data
	player_snapshot_timeline = []
	player_snapshot_timeline.resize(DEFAULT_TIMELINE_SIZE)
	player_snapshot_timeline.fill({})


func _physics_process(_delta):
	if multiplayer.has_multiplayer_peer() and multiplayer.is_server():
		var multiplayer_player_info = State.get_state("multiplayer_server_only_player_info")
		var server_physics_tick = Engine.get_physics_frames()

		var players = []
		for peer_id in multiplayer_player_info.keys():
			var peer_info = multiplayer_player_info[peer_id]

			if peer_info != null:
				var player = multiplayer_player_info[peer_id]["player"]
				if player != null:
					players.append(player.get_multiplayer_pull())

		self.pull_sync_player_entity.rpc(server_physics_tick, players)


###########################################################
# ENTITY SYNC STUFF
###########################################################

# TODO: remove next_global_transform in the future or ignore it, for now client can manipulate their own position on their own will
#       this is a temporary measure, it WILL ALLOW VERY EASY CLIENT SIDE TRANSFORM MANIPULATION aka. CHEATING.
# client_trust_data structure:
# {
# 	"next_global_transform": <transform 3D of player>
# }
@rpc("any_peer", "call_remote", "unreliable_ordered")
func push_sync_player_entity(server_physics_tick_that_client_uses: int, input: Vector3, actions: Array, camera_transform: Transform3D, parent_pivot_transform: Transform3D, current_weapon_slot: int, client_trust_data: Dictionary) -> void:
	if not multiplayer.is_server():
		return

	var peer_id = multiplayer.get_remote_sender_id()
	var current_state = State.get_state("multiplayer_server_only_player_info")
	var player = current_state[peer_id]["player"]

	if player == null:
		printerr("ERROR: Player Pushing Entity Data When Dead", get_stack())

	if player != null:
		var snapshot_data = DEFAULT_PLAYER_SNAPSHOT.duplicate(true)
		snapshot_data["global_transform"] = player.global_transform
		snapshot_data["velocity"] = player.velocity

		self.populate_timeline_snapshot(Engine.get_physics_frames(), peer_id, snapshot_data)
		player.set_multiplayer_pull(server_physics_tick_that_client_uses, input, actions, camera_transform, parent_pivot_transform, current_weapon_slot, {})


# TODO: remove next_global_transform in the future or ignore it, for now client can manipulate their own position on their own will
#       this is a temporary measure, it WILL ALLOW VERY EASY CLIENT SIDE TRANSFORM MANIPULATION aka. CHEATING.
#
# players: [
#   [<peer_id>, <input>, <pivot_transform>, <client_trust_data>]
# ]
@rpc("authority", "call_remote", "unreliable_ordered")
func pull_sync_player_entity(server_phyics_tick: int, players) -> void:
	var current_state = State.get_state("multiplayer_server_only_player_info")

	# build client_tick_to_compare before physics tick cache values are updated
	var client_tick_to_compare = 0
	if self.multiplayer_latest_server_physics_tick > 0:
		client_tick_to_compare = (server_phyics_tick - self.multiplayer_latest_server_physics_tick) + self.multiplayer_latest_client_physics_tick_when_server_info_comes

	# update physics tick cache
	self.multiplayer_latest_server_physics_tick = server_phyics_tick
	self.multiplayer_latest_client_physics_tick_when_server_info_comes = Engine.get_physics_frames()

	for player_data in players:
		var peer_id: int = player_data[0]

		# from data
		var _time: float = player_data[1] # for now we're not using this, maybe in the future for tickless? lmao.
		var movement_input: Vector3 = player_data[2]
		var actions: Array = player_data[3]
		var camera_transform: Transform3D = player_data[4]
		var parent_pivot_transform: Transform3D = player_data[5]
		var current_weapon_slot: int = player_data[6]
		var client_trust_data: Dictionary = player_data[7]

		var peer_info = current_state[peer_id]
		if peer_info != null:
			var player = current_state[peer_id]["player"]
			if player != null:

				# only populate yourself for client prediction (to make things smooth)
				if player.is_owned_by_me():
					var snapshot_data = DEFAULT_PLAYER_SNAPSHOT.duplicate(true)
					snapshot_data["global_transform"] = Transform3D(player.global_transform)
					snapshot_data["velocity"] = Vector3(player.velocity)
					self.populate_timeline_snapshot(Engine.get_physics_frames(), peer_id, snapshot_data)

				player.set_multiplayer_pull(client_tick_to_compare, movement_input, actions, camera_transform, parent_pivot_transform, current_weapon_slot, client_trust_data)


func build_client_trust_data(next_global_transform: Transform3D) -> Dictionary:
	return {
		"next_global_transform": next_global_transform
	}


# will return with -1 if physics_frame_count is invalid
# NOTE: if invalid, the caller should instead sync everything based on the latest snapshot available
func build_timeline_snapshot_index(physics_frame_count) -> int:
	if physics_frame_count > Engine.get_physics_frames():
		return -1

	var oldest_physics_frame_in_buffer = max(Engine.get_physics_frames() - self.player_snapshot_timeline.size() + 1, 0)
	if oldest_physics_frame_in_buffer > physics_frame_count:
		return -1

	return physics_frame_count % self.player_snapshot_timeline.size()


# will return empty dictionary when physics_frame_count is invalid
func get_timeline_snapshot(physics_frame_count: int) -> Dictionary:
	var index = self.build_timeline_snapshot_index(physics_frame_count)
	if index < 0:
		return {}

	return self.player_snapshot_timeline[index]


# the snapshot timeline is used client and server side with several differences.
# SERVER-SIDE:
#   - all players' state are snapshoted per frame
#   - rollback are used for shooting mechanic for example, where client might see an enemy several frames before the current server frame.
# CLIENT-SIDE:
#   - only snapshot the currently owned player
#   - used for client side prediction, movement is done instantly on the client, but will be snapped back if it deviates too much
func populate_timeline_snapshot(physics_frame_count: int, peer_id: int, snapshot_data: Dictionary) -> void:
	var index = physics_frame_count % self.player_snapshot_timeline.size()
	self.player_snapshot_timeline[index][peer_id] = snapshot_data


###########################################################
# GAME MODE GENERAL
###########################################################

func handle_added_player(player: Node):
	# Connect i health when dead and add the player themselves as binding value
	if "i_health" in player:
		player.i_health.connect("dead",Callable(self,"_on_player_ihealth_dead").bind(player))


func handle_new_level(level: Node):
	var game_mode = State.get_state("game_mode")

	match game_mode:
		Global.GAME_MODE.DEFAULT:
			gmd_handle_new_level(level)
		Global.GAME_MODE.AIM_TRAINING, Global.GAME_MODE.CUSTOM:
			print(Global.LOG_INFO, "This game mode is handled by the level script not by game_mode.gd")
		_:
			print_stack()
			printerr("Game Mode Doesn't Exist")


###########################################################
# GAME MODE SECTION
#
# gmd  => Game Mode DEFAULT
# glc  => Game Mode LONG_COMPETITIVE
# gsc  => Game Mode SHORT_COMPETITIVE
###########################################################


func gmd_handle_new_level(_level):
	gmd_add_player_server()


func gmd_add_player_server() -> void:
	if Lobby.is_authoritative():
		# Add To World3D
		# Consequently, will be handled by handle_added_player automagically in _on_tree_node_added
		var universal_spawn_points = get_tree().get_nodes_in_group(Global.SPAWN_TYPE_GROUP_UNIVERSAL)
		var peers = PackedInt32Array([0])

		if Lobby.is_lobby_master():
			peers = multiplayer.get_peers()
			peers.append(multiplayer.get_unique_id())

		for i in range(peers.size()):
			var peer_id = peers[i]
			var global_spawn_point = universal_spawn_points[wrapi(i, 0, len(universal_spawn_points))].global_transform

			if multiplayer.has_multiplayer_peer():
				self.gmd_add_player.rpc(global_spawn_point, peer_id)
			if not multiplayer.has_multiplayer_peer():
				self.gmd_add_player(global_spawn_point, peer_id)


@rpc("authority", "call_local", "reliable")
func gmd_add_player(global_spawn_point: Transform3D, peer_id: int) -> void:
	# Add To World3D
	# Consequently, will be handled by handle_added_player automagically in _on_tree_node_added
	var new_player = player_scene.instantiate()
	new_player.global_transform = global_spawn_point
	new_player.owned_by_peer_id = peer_id

	var current_state = State.get_state("multiplayer_server_only_player_info").duplicate(true) # TODO: can we actually just duplicate a reference to a node?
	current_state[peer_id] = {
		"player": new_player
	}
	State.set_state("multiplayer_server_only_player_info", current_state)

	Util.add_to_world(new_player)


func gmd_handle_player_dead(player):
	player.queue_free()

	# Resurrect Them
	# TODO: only work for single player
	var universal_spawn_point = get_tree().get_nodes_in_group(Global.SPAWN_TYPE_GROUP_UNIVERSAL).pop_front()
	var new_player = player_scene.instantiate()
	new_player.global_transform = universal_spawn_point.global_transform

	# Add To World3D
	# Consequently, will be handled by handle_added_player automagically in _on_tree_node_added
	Util.add_to_world(new_player)


###########################################################
# SIGNAL HANLDER
###########################################################

func _on_tree_node_added(node: Node) -> void:
	if Util.is_in_either_group(node, player_group_names):
		await node.ready # Node must be ready before processing
		handle_added_player(node)
	if node.is_in_group(Global.GROUP_LEVEL):
		await node.ready
		handle_new_level(node)


func _on_player_ihealth_dead(player):
	var game_mode = State.get_state("game_mode")

	match game_mode:
		Global.GAME_MODE.DEFAULT:
			gmd_handle_player_dead(player)
		_:
			print_stack()
			printerr("Game Mode Doesn't Exist")
