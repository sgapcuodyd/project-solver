extends Node

###################################################################################################
# CLIENT & SERVER
###################################################################################################


func _ready():
	multiplayer.multiplayer_peer = null
	multiplayer.peer_connected.connect(_peer_connected)
	multiplayer.peer_disconnected.connect(_peer_disconnected)
	multiplayer.server_disconnected.connect(_server_disconnected)


func _physics_process(_delta):
	var skip_pool = self.build_is_skip_pooling(Engine.get_physics_frames(), Global.NETWORK_DELAY_FRAMES)
	if multiplayer.has_multiplayer_peer() and not skip_pool:
		multiplayer.poll()


func _peer_connected(peer_id: int) -> void:
	print(Global.LOG_INFO, "peer %d is connected, is_server: %s" % [peer_id, multiplayer.is_server()])


func _peer_disconnected(peer_id: int) -> void:
	print(Global.LOG_INFO, "peer %d is disconnected, is_server: %s" % [peer_id, multiplayer.is_server()])

	if multiplayer.is_server():
		var current_info = State.get_state("multiplayer_lobby_player_info").duplicate(true)
		current_info.erase(peer_id)

		State.set_state("multiplayer_lobby_player_info", current_info)
		self.set_lobby_information.rpc(current_info)

	if not multiplayer.is_server():
		self.force_to_main_menu() # happens when server is stopped


# TODO: for now server == lobby master, change this in the future.
func is_lobby_master() -> bool:
	return multiplayer.has_multiplayer_peer() and multiplayer.is_server()


# will return true if it's lobby master or is in single player mode
func is_authoritative() -> bool:
	return self.is_lobby_master() or not multiplayer.has_multiplayer_peer()

func leave_lobby() -> void:
	State.set_state("multiplayer_lobby_is_entered", false)
	State.set_state("multiplayer_lobby_player_info", {})

	if multiplayer.has_multiplayer_peer():
		if multiplayer.is_server():
			self.force_to_main_menu.rpc()

		# make it null at the very last
		# timer is used because a server might need to send the rpc first
		await get_tree().create_timer(2.0, true, true).timeout
		multiplayer.multiplayer_peer = null



func kick_player_from_lobby(peer_id: int) -> void:
	if multiplayer.multiplayer_peer:
		multiplayer.multiplayer_peer.disconnect_peer(peer_id)


func build_is_skip_pooling(current_physics_tick: int, skip_frame: int) -> bool:
	return skip_frame > 0 and (current_physics_tick % skip_frame) == 0


###################################################################################################
# SERVER
###################################################################################################


# Create a server and enter the lobby immediately.
# Useful for self hosting on your own computer, but still want to play.
# For dedicated servers, you may use create_server instead.
func create_server_and_enter_lobby(nametag: String, password: String, port: int) -> void:
	self.create_server(port)

	var uid = multiplayer.get_unique_id()
	State.set_state("multiplayer_lobby_is_entered", true)
	State.set_state("multiplayer_lobby_password", password)
	State.set_state("multiplayer_lobby_player_info", {
		uid: {
			"nametag": nametag
		}
	})


func create_server(port: int) -> void:
	if multiplayer.has_multiplayer_peer():
		multiplayer.multiplayer_peer = null

	var peer = ENetMultiplayerPeer.new()
	var err = peer.create_server(port)
	if err != OK:
		Util.handle_err(err)
		return

	peer.host.compress(ENetConnection.COMPRESS_RANGE_CODER)
	multiplayer.multiplayer_peer = peer
	get_tree().multiplayer_poll = false


# Enter a lobby, only used by clients.
# Self hosting server should instead use create_server_and_enter_lobby.
# BUG: password should be questioned by the server to the client instead of the other way around
#      flow:
#          1. client connect to server
#          2. server get the peer id with _peer_connected
#          3. still in _peer_connected, server rpc the client with interogate_password.rpc() that returns a string
#          4. still in _peer_connected, use the string and authorize it with the one we have
#          5. if correct, don't disconnect the client
@rpc("any_peer", "call_remote", "reliable")
func enter_lobby(nametag: String, password: String) -> void:
	if not multiplayer.is_server():
		return

	var uid = multiplayer.get_remote_sender_id()
	if State.get_state("multiplayer_lobby_password") != password:
		multiplayer.multiplayer_peer.disconnect_peer(uid)

	var current_info = State.get_state("multiplayer_lobby_player_info").duplicate(true)
	current_info[uid] = {
		"nametag": nametag
	}

	State.set_state("multiplayer_lobby_player_info", current_info)
	self.set_lobby_information.rpc(current_info)


###################################################################################################
# CLIENT
###################################################################################################

func connect_and_enter_lobby(nametag: String, ip_address: String, port: int, password: String) -> void:
	var err = connect_to_server(ip_address, port)
	if err != OK:
		# TODO: alert the user of connection errors by UI
		Util.handle_err(err)
		return

	await multiplayer.connected_to_server

	State.set_state("multiplayer_lobby_is_entered", true)
	enter_lobby.rpc_id(MultiplayerPeer.TARGET_PEER_SERVER, nametag, password)


# will return the error when connecting to server
func connect_to_server(ip_address: String, port: int) -> int:
	var err = 0

	if multiplayer.has_multiplayer_peer():
		multiplayer.multiplayer_peer = null

	var peer = ENetMultiplayerPeer.new()
	err = peer.create_client(ip_address, port)
	if err != OK:
		Util.handle_err(err)
		return err

	peer.host.compress(ENetConnection.COMPRESS_RANGE_CODER)
	multiplayer.multiplayer_peer = peer
	get_tree().multiplayer_poll = false

	return err


@rpc("authority", "call_remote", "reliable")
func set_lobby_information(server_lobby_info: Dictionary) -> void:
	if multiplayer.is_server():
		return

	State.set_state("multiplayer_lobby_player_info", server_lobby_info)


@rpc("authority", "call_local", "reliable")
func load_map(map_path: String, game_mode: int) -> void:
	Util.change_level(map_path, game_mode)


@rpc("authority", "call_remote", "reliable")
func force_to_main_menu() -> void:
	Util.change_level("res://ui/main_menu.tscn")


func _server_disconnected() -> void:
	self.leave_lobby()
